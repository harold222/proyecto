import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/destinoViaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  fg: FormGroup;
  restriccion = "antartida";
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      destino: ['', Validators.compose([
        Validators.required,
        this.longitudValidator,
        this.restriccionValidator(this.restriccion)
      ])],
      pais: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Ha cambiado el formulario: ', form);
    })
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('destino');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(120),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, pais: string) {  
    const destino = new DestinoViaje(nombre, pais);
    this.onItemAdded.emit(destino);
    return false;
  }

  longitudValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;

    if (l > 0 && l < 3) {
      return {
        invalidLong: true
      }
    }

    return null;
  }

  restriccionValidator(nombre: string): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const n = control.value.toString().trim();

      if (n === nombre) {
        return { restrictedDestino: true }
      }

      return null;
    }
  }

}
