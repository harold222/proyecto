import { Component, OnInit } from '@angular/core';
import { ReservasApiClientService } from '../reservas-api-client.service';

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css']
})
export class ReservasListadoComponent implements OnInit {
  constructor(private _api: ReservasApiClientService) { }

  ngOnInit(): void {
  }

  public get api(): ReservasApiClientService {
    return this._api;
  }
  
  public set api(value: ReservasApiClientService) {
    this._api = value;
  }

}
