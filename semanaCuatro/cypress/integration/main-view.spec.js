describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
      cy.visit('http://localhost:4200');
      cy.contains('Tarea Módulo 4');
      cy.get('h4 b').should('contain', 'HOLAes');
    });
});