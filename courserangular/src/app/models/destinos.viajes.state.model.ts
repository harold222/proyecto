
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DestinoViaje, Servicio } from './destino-viaje.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initialDestinosViajesState() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

// ACCIONES
export enum DestinosActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data',
    QUITAR_DESTINO = '[Destinos] Quitar',
    VOTE_UP = '[Destinos] Vote Up',
    VOTE_DOWN = '[Destinos] Vote Down',
    VOTE_STATE_UP = '[Destinos] Vote State Up',
    VOTE_STATE_DOWN = '[Destinos] Vote State Down'
}

export class NuevoDestinoAction implements Action {
    type = DestinosActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class QuitarDestinoAction implements Action {
    type = DestinosActionTypes.QUITAR_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
    
}

export class VoteDownAction implements Action {
    type = DestinosActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export class VoteStateUpAction implements Action {
    type = DestinosActionTypes.VOTE_STATE_UP;
    constructor(public destino: DestinoViaje, public servicio: Servicio) { }
}

export class VoteStateDownAction implements Action {
    type = DestinosActionTypes.VOTE_STATE_DOWN;
    constructor(public destino: DestinoViaje, public servicio: Servicio) {}
}

export class InitMyDataAction implements Action {
    type = DestinosActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
}

export type DestinosActions = NuevoDestinoAction | QuitarDestinoAction
    | ElegidoFavoritoAction | InitMyDataAction| VoteUpAction | VoteDownAction | VoteStateDownAction | VoteStateUpAction;


// REDUCERS
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosActions
): DestinosViajesState {
    switch (action.type) {
        case DestinosActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, '',0,true))
              };
          }
        case DestinosActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosActionTypes.ELEGIDO_FAVORITO: {
            let destinos = state.items.map(x => ({...x}));
            destinos.forEach(x => x.id == (action as ElegidoFavoritoAction).destino.id ? x.selected = true : x.selected = false);
            console.log(destinos);
            let fav: DestinoViaje = <DestinoViaje>destinos.find(d => d.id == (action as ElegidoFavoritoAction).destino.id);
            console.log(fav);
            fav.selected = true;
            console.log(fav);
            return {
              ...state,
              favorito: fav,
              items: <DestinoViaje[]>destinos
            };
        }
         case DestinosActionTypes.QUITAR_DESTINO: {
            console.log('destinosReducer - QUITAR_DESTINO');
            let destinos = state.items.map(x => ({...x}));
            console.log(destinos);
            const d: DestinoViaje = (action as QuitarDestinoAction).destino;
            console.log(d);
            const index = destinos.map(function(i){ return i.id; }).indexOf(d.id);
            console.log(index);
            destinos.splice(index, 1);
            console.log(destinos);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            }
        }
        case DestinosActionTypes.VOTE_UP: {
            console.log('destinosReducer - VOTE_UP');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteUpAction).destino.id);
            d.votes++;
            console.log(state);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        }
        case DestinosActionTypes.VOTE_DOWN: {
            console.log('destinosReducer - VOTE_DOWN');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteDownAction).destino.id);
            if(d.votes > 0) {
                d.votes--;
            }
            console.log(state);
            console.log(d);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        } 
        case DestinosActionTypes.VOTE_STATE_UP: {
            console.log('destinosReducer - VOTE_STATE_UP');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteStateUpAction).destino.id);
            let servicios = d.servicios.map(x => ({...x}));
            let s: Servicio = <Servicio>servicios.find(s => s.id === (action as VoteStateUpAction).servicio.id);
            s.voto++;
            d.servicios = <Servicio[]>servicios;
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        }
        case DestinosActionTypes.VOTE_STATE_DOWN: {
            console.log('destinosReducer - VOTE_STATE_DOWN');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteStateDownAction).destino.id);
            let servicios = d.servicios.map(x => ({...x}));
            let s: Servicio = <Servicio>servicios.find(s => s.id === (action as VoteStateDownAction).servicio.id);
            if(s.voto > 0) {
                s.voto--;
                d.servicios = <Servicio[]>servicios;
            }
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        } 
    }

    return state;
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado : Observable<Action> = this.actions$.pipe(
        ofType(DestinosActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions) {

    }
}
