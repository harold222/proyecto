import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinosAPiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';

class DestinosAPiClientViejo {
  getById(id: string): DestinoViaje {
    console.log('Clase vieja');
    return null
  }
}

interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

/* @Injectable({
  providedIn: 'root'
})
class DestinoApiClientDecorated extends DestinosAPiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }
  getById(id: string): DestinoViaje {
    console.log('llamado por la clase decorada');
    console.log('config: ' + this.config.apiEndPoint);
    return super.getById(id);
    
  }
} */

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosAPiClient,
   /*  { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }, 
    { provide: DestinosAPiClient, useClass: DestinoApiClientDecorated }, 
    { provide: DestinosAPiClientViejo, useExisting: DestinosAPiClient}  */
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosAPiClientViejo) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
