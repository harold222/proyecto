import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasdetalleComponent } from './reservasdetalle/reservasdetalle.component';


const routes: Routes = [
  { 
    path: 'reservas', component: ReservasListadoComponent
  },
  { 
    path: 'reservas/:id', component: ReservasdetalleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
