import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje, Servicio } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction, VoteStateUpAction, VoteStateDownAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input('idx') position: number;
  @Output() clicked:EventEmitter<DestinoViaje>;
  @Output() deleted:EventEmitter<DestinoViaje>;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
    this.deleted = new EventEmitter();
  }

  ngOnInit(): void {
  }

  seleccionar() {
    console.log('seleccionar...');
    this.clicked.emit(this.destino);

    return false;
  }

  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  quitar() {
    this.deleted.emit(this.destino);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino))
    return false;
  }

  voteServiceUp(servicio: Servicio){
    this.store.dispatch(new VoteStateUpAction(this.destino, servicio));
    return false;
  }

  voteServiceDown(servicio: Servicio){
    this.store.dispatch(new VoteStateDownAction(this.destino, servicio))
    return false;
  }
}
