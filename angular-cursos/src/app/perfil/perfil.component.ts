import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Perfil } from '../models/perfil.model';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  nombre: string;
  nickname: string;

  @Output() tutor = new EventEmitter();
  @Input() perfil: Perfil;
  @Input() materiasRec: string[];
  constructor() {
    this.nombre = "Marcos";
    this.nickname = "mst7";
   }

  ngOnInit(): void {
    this.tutor.emit(this.nombre);
  }

  actualizarNick(pNick: string){
    this.nickname = pNick;
  }

  actualizarName(pName: string){
    this.nombre = pName;
  }
}
