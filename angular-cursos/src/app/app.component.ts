import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-cursos';
  tutor: string = "";
  materias: string[];

  recibeTutor(ev:any){
    this.tutor = ev;
    console.log(ev);
    
  }

  recibeMaterias(mat:any){
    this.materias = mat;
    console.log(mat);
  }
}
