export class Perfil{
    nombre: string;
    nickname: string;

    constructor(pNombre:string, pNickName: string){
        this.nombre = pNombre;
        this.nickname = pNickName;
    }
}