import { Perfil } from './perfil.model';

export class Curso{
    private selected: boolean;
    constructor(public nombre:string, public tutor:string, public imagen:string){  }

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(){
        this.selected = true;
    }
}